class Node(object):
    def __init__(self, id, reward):
        self.id = id
        self.reward = reward
        self.paths = {}
        self.finish = False

    def availablePaths(self):
        return self.paths

    def setPath(self, direction, value):
        self.paths[direction] = value
        
        
class Map:
    def __init__(self, size_Y, size_X):
        self.node_map = []
        self.size_X = size_X
        self.size_Y = size_Y
        self.current_pos = (0,0)
        self.setup()

    def setup(self):
        # Initializing Nodes
        self.node_map = [ [ None for x in range( self.size_X ) ] for y in range( self.size_Y ) ]
        #print(self.node_map)
        for x in range(self.size_X):
            for y in range(self.size_Y):
                self.node_map[y][x] = Node((y,x), -0.04)

        # Adding Right/Left paths
        for row in self.node_map:
            for a in range(self.size_X - 1):
                row[a].setPath("R", 0)
            for a in range(self.size_X - 1,1,-1):
                row[a].setPath("L", 0)

        # Adding Down paths
        for y in range(self.size_Y - 1):
            for x in range(self.size_X):
                self.node_map[y][x].setPath("D", 0)

        # Adding Up paths
        for y in range(1, self.size_Y):
            for x in range(self.size_X):
                self.node_map[y][x].setPath("U", 0)

    # Set map parameters
    def setPosition(self, y, x):
        self.current_pos = (y,x)

    def setGoal(self, y, x, r):
        self.node_map[y][x].reward = r
        self.node_map[y][x].finish = True

    def setHole(self, y, x, r):
        self.node_map[y][x].reward = r
        self.node_map[y][x].finish = True

    def setWall(self, x, y, r): # TODO: Implement on next iteration
        pass

    # Movement
    def tryMove(self, direction): # TODO: Triple check if the direction is available
        y,x = self.current_pos
        if direction == "U":
            return self.node_map[y-1][x]
        elif direction == "D":
            return self.node_map[y+1][x]
        elif direction == "R":
            return self.node_map[y][x+1]
        elif direction == "L":
            return self.node_map[y][x-1]
        else:
            print("Direction not known")
            exit(1)






    
    
