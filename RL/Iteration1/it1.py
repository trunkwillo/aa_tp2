# Heavily based on: https://www.youtube.com/watch?v=1XRahNzA5bE
import world
import numpy as np

# Initializing
size_X = 5
size_Y = 5
wrld = world.Map(size_Y, size_X)
# Initial state set (0,0)
wrld.setPosition(0,0)
wrld.setHole(1,0,-1)
wrld.setHole(1,1,-1)
wrld.setHole(1,2,-1)
wrld.setHole(1,3,-1)
wrld.setHole(2,3,-1)
wrld.setHole(3,3,-1)
wrld.setHole(4,3,-1)
# Final state set (4,4)
wrld.setGoal(4,4, 1)

# Learning rate
alpha = 0.5
ypsillon = 0.99

for t in range(10000):
        # Reset to starting position
        wrld.setPosition(0,0)
        while(True):
                # Check whether state is a finish TODO: Finish this part
                current_Y, current_X = wrld.current_pos
                current_node = wrld.node_map[current_Y][current_X]
                if current_node.finish:
                        break
                # See available actions
                possible_actions = current_node.availablePaths()
                # Pick the one with the best Q value (random if multiple the same)
                best_action = max(possible_actions, key=possible_actions.get)
                next_node = wrld.tryMove(best_action)
                # Update it's value to the formula: Q(state, action) += alpha*(R(next_state) + ypsillon* max(Q(next_state, action)) - Q(state, action))
                # Q(state, action)
                current_Q = current_node.paths[best_action]
                # R(next_state)
                reward = next_node.reward
                # Max(Q(next_sate, action))
                next_actions = next_node.availablePaths()
                next_action = max(next_actions, key=next_actions.get)
                max_value = next_node.paths[next_action]
                # Equation
                new_value = current_Q + alpha * (reward + ypsillon * max_value - current_Q)
                wrld.node_map[current_Y][current_X].setPath(best_action, new_value)
                # Change to the next state
                next_Y, next_X = next_node.id
                wrld.setPosition(next_Y, next_X)



# Check all the values
print("Q values: ")
for y in range(size_Y):
        for x in range(size_X):
                print("Y: ", y, " X: ", x,wrld.node_map[y][x].availablePaths())

# Check the chosen path
print("Chosen final path: ")
wrld.setPosition(0,0)
chosen_path = []
while(True):
        # Check if current node is the end
        current_Y, current_X = wrld.current_pos
        chosen_path += [(current_Y, current_X)]
        current_node = wrld.node_map[current_Y][current_X]
        if current_node.finish:
                break
        # See available actions
        possible_actions = current_node.availablePaths()
        # Pick the one with the best Q value (random if multiple the same)
        best_action = max(possible_actions, key=possible_actions.get)
        next_node = wrld.tryMove(best_action)
        # Change to the next state
        next_Y, next_X = next_node.id
        wrld.setPosition(next_Y, next_X)

print(chosen_path)
