# Heavily based on: https://www.youtube.com/watch?v=1XRahNzA5bE
import world
import numpy as np
import random
import time

# Initializing
# Going to add a 3rd dimension to the states (velocity, 0 equals slow (+/- 30 Km/h), 1 equals fast (+/- 50 Km/h))
size_X = 5
size_Y = 5
wrld = world.Map(size_Y, size_X)
# Initial state set (0,0)
wrld.setPosition(0,0,0)

# Final state set (4,4)
wrld.setGoal(4,4,1)

# Set walls/holes pattern
# wrld.setHole(1,0,-1)
# wrld.setHole(1,1,-1)
# wrld.setHole(1,2,-1)
# wrld.setHole(1,3,-1)
# wrld.setHole(2,3,-1)
# wrld.setHole(3,3,-1)
# wrld.setHole(4,3,-1)

# Check all directions
# print("All directions: ")
# for y in range(size_Y):
#         for x in range(size_X):
#                 print("Y: ", y, " X: ", x, wrld.node_map[y][x][1].availablePaths().keys())


# Learning rate
alpha = 0.5
gamma = 0.99
epoch = 100000
random_threshold = 0.5
al = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9 , 1]
ga = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9 , 0.95, 1]
for a in al:
    for g in ga:
        alpha = a
        gamma = g
        start_time = time.time()
        print("Alpha: ", a, " Gamma: ", y)
        for _ in range(epoch):
                # if(t % (epoch/10)) == 0:
                #         prog = t / (epoch/10)
                #         print("Progress: ", int(prog), "/ 10")
                # Reset to starting position
                wrld.setPosition(0,0,1)
                while(True):
                        # Check whether state is a finish TODO: Finish this part
                        current_Y, current_X, current_V = wrld.current_pos
                        current_node = wrld.node_map[current_Y][current_X][current_V]
                        if current_node.finish:
                                break
                        # See available actions
                        possible_actions = current_node.availablePaths()
                        # Pick the one with the best Q value (random if multiple the same)
                        if random.random() > random_threshold:
                                best_action = max(possible_actions, key=possible_actions.get)
                        else:
                                best_action = random.choice(list(possible_actions.keys()))
                        next_node = wrld.tryMove(best_action)
                        # Update it's value to the formula: Q(state, action) += alpha*(R(next_state) + gamma* max(Q(next_state, action)) - Q(state, action))
                        # Q(state, action)
                        current_Q = current_node.paths[best_action]
                        # R(next_state)
                        reward = next_node.reward
                        # Max(Q(next_sate, action))
                        next_actions = next_node.availablePaths()
                        next_action = max(next_actions, key=next_actions.get)
                        max_value = next_node.paths[next_action]
                        # Equation
                        new_value = current_Q + alpha * (reward + gamma * max_value - current_Q)
                        wrld.node_map[current_Y][current_X][current_V].setPath(best_action, new_value)
                        # Change to the next state
                        next_Y, next_X, next_V = next_node.id
                        wrld.setPosition(next_Y, next_X, next_V)

        # Check all the values
        # print("Q values: ")
        # for y in range(size_Y):
        #         for x in range(size_X):
        #             for v in range(2):
        #                 print("Y: ", y, " X: ", x, " V: ", v, wrld.node_map[y][x][v].availablePaths())

        # Check the chosen path
        # print("Chosen final path: ")
        wrld.setPosition(0,0,0)
        total_cost = 0
        chosen_path = []
        while(True):
                # Check if current node is the end
                current_Y, current_X, current_V = wrld.current_pos
                chosen_path += [(current_Y, current_X, current_V)]
                current_node = wrld.node_map[current_Y][current_X][current_V]
                if current_node.finish:
                        break
                # See available actions
                possible_actions = current_node.availablePaths()
                # Pick the one with the best Q value (random if multiple the same)
                best_action = max(possible_actions, key=possible_actions.get)
                next_node = wrld.tryMove(best_action)
                # Change to the next state
                total_cost += next_node.reward
                next_Y, next_X, next_V = next_node.id
                wrld.setPosition(next_Y, next_X, next_V)

        elapsed_time = time.time() - start_time
        print("Chosen path: ",chosen_path)
        print("Total Cost: ", total_cost)
        print("Time spent: ", elapsed_time, "\n")
