class Node(object):
    def __init__(self, id, reward):
        self.id = id
        self.reward = reward
        self.paths = {}
        self.finish = False

    def availablePaths(self):
        return self.paths

    def setPath(self, direction, value):
        self.paths[direction] = value


class Map:
    def __init__(self, size_Y, size_X):
        self.node_map = []
        self.size_X = size_X
        self.size_Y = size_Y
        self.size_V = 2
        self.v = 0
        self.current_pos = (0,0,0)
        # Parameters
        self.slow_reward = -0.02
        self.fast_reward = -0.01
        # Setup
        self.setup()

    def setup(self):
        # Initializing Nodes (Y,X,V)
        self.node_map =  [ [ [ None for x in range( self.size_V ) ] for x in range( self.size_X ) ] for y in range( self.size_Y ) ]
        # print(self.node_map)
        for x in range(self.size_X):
            for y in range(self.size_Y):
                self.node_map[y][x][0] = Node((y,x,0), self.slow_reward)
                self.node_map[y][x][1] = Node((y,x,1), self.fast_reward)

        # Adding Right/Left paths
        for row in self.node_map:
            for a in range(self.size_X - 1):     
                row[a][0].setPath("RS", 0)
                row[a][0].setPath("RF", 0)
                row[a][1].setPath("RS", 0)
                row[a][1].setPath("RF", 0)
            for a in range(1,self.size_X):
                row[a][0].setPath("LS", 0)
                row[a][0].setPath("LF", 0)
                row[a][1].setPath("LS", 0)
                row[a][1].setPath("LF", 0)
                  

        # Adding Down paths
        for y in range(self.size_Y - 1):
            for x in range(self.size_X):
                self.node_map[y][x][0].setPath("DS", 0)
                self.node_map[y][x][0].setPath("DF", 0)
                self.node_map[y][x][1].setPath("DS", 0)           
                self.node_map[y][x][1].setPath("DF", 0)
                
                

        # Adding Up paths
        for y in range(1, self.size_Y):
            for x in range(self.size_X):
                self.node_map[y][x][0].setPath("US", 0)
                self.node_map[y][x][0].setPath("UF", 0)
                self.node_map[y][x][1].setPath("US", 0)
                self.node_map[y][x][1].setPath("UF", 0)
                
                



    # Set map parameters
    def setPosition(self, y, x, v):
        self.current_pos = (y,x,v)

    def setGoal(self, y, x, r):
        self.node_map[y][x][0].reward = r
        self.node_map[y][x][0].finish = True
        self.node_map[y][x][1].reward = r
        self.node_map[y][x][1].finish = True

    def setHole(self, y, x, r): # AKA: Wall
        self.node_map[y][x][0].reward = r
        self.node_map[y][x][0].finish = True
        self.node_map[y][x][1].reward = r
        self.node_map[y][x][1].finish = True

    # Movement
    def tryMove(self, direction): # TODO: Triple check if the direction is available
        y,x,v = self.current_pos
        if direction == "US":
            return self.node_map[y-1][x][0]
        elif direction == "UF":
            return self.node_map[y-1][x][1]
        elif direction == "DS":
            return self.node_map[y+1][x][0]
        elif direction == "DF":
            return self.node_map[y+1][x][1]
        elif direction == "RS":
            return self.node_map[y][x+1][0]
        elif direction == "RF":
            return self.node_map[y][x+1][1]
        elif direction == "LS":
            return self.node_map[y][x-1][0]
        elif direction == "LF":
            return self.node_map[y][x-1][1]
        else:
            print("Direction not known")
            exit(1)
