# Heavily based on: https://www.youtube.com/watch?v=1XRahNzA5bE
import world
import numpy as np
import random
import itertools
import time

print("Setting up...")

# TODO: Test complex scenario on multiple values
punish_mp = [0, 0.05, 0.1, 0.2]
al = [0.25, 0.5, 0.75]
ga = [0.25, 0.5, 0.75, 0.99]
rnd = [0, 0.25, 0.5]
epochs = [1000, 10000]
steps = [1000, 10000]

# Generate a list with all combinations
all = [punish_mp, al, ga, rnd, epochs, steps]
comb = list(itertools.product(*all))
print(len(comb), "cases testing.")



for par in comb:
    # Initializing
    # Going to add a 3rd dimension to the states (velocity, 0 equals slow (+/- 30 Km/h), 1 equals fast (+/- 50 Km/h))
    punish, alpha, gamma, random_threshold, epochs, max_steps = par
    base_pop = 0
    size_X = 100
    size_Y = 100
    wrld = world.Map(size_Y, size_X, base_pop, punish)
    # Initial state set (0,0)
    wrld.setPosition(0,0,0)
    wrld.setStart(0,0)
    wrld.setGoal(99,49,0)

    # Set city blocks (check report for diagram)
    # City Block 1
    for i in range(1,50):
        wrld.setWall(i, i-1)
        wrld.setWall(i, 0)
        wrld.setWall(49, i-1)

    # City Block 2
    for i in range(49):
        wrld.setWall(i, i + 2)
        wrld.setWall(0, i + 2)
        wrld.setWall(i, 50)

    # City Block 3
    for i in range(50,100):
        wrld.setWall(49, i)

    # City Block 4
    for i in range(49, 100):
        wrld.setWall(i, 28)

    # City Block 5
    for i in range(51,74):
        wrld.setWall(i,30)
        wrld.setWall(i,68)
    for i in range(30, 69):
        wrld.setWall(51,i)
        wrld.setWall(73,i)

    # City Block 6
    for i in range(75,99):
        wrld.setWall(i,30)
        wrld.setWall(i,48)
    for i in range(30, 49):
        wrld.setWall(75,i)
        wrld.setWall(98,i)

    # City Block 7
    for i in range(75,99):
        wrld.setWall(i,50)
        wrld.setWall(i,68)
    for i in range(50, 69):
        wrld.setWall(75,i)
        wrld.setWall(98,i)

    # City Block 8
    for i in range(49,100):
        wrld.setWall(i,70)

    # Population Density
    for i in range(93,100):
        wrld.setDensity(i,29,5)
    for i in range(30,35):
        wrld.setDensity(99,i,5)

    for i in range(67,84):
        wrld.setDensity(i,69,5)
    for i in range(65,70):
        wrld.setDensity(74,i,5)

    #wrld.saveMap2Image() # UNCOMMENT HERE TO SEE FINAL MAP RESULT IN IMAGE

    #wrld.setWall(50,51)
    # Check all directions
    # print("All directions: ")
    # for y in range(size_Y):
    #         for x in range(size_X):
    #                 print("Y: ", y, " X: ", x, wrld.node_map[y][x][1].availablePaths().keys())


    # Learning rate
    # alpha = 0.5
    # gamma = 0.99
    # epoch = 100000
    # max_steps = 1000
    # random_threshold = 0.2
    print("Starting the training...")
    start_time = time.time()
    for t in range(1,epochs + 1):
            if(t % (epochs/10)) == 0:
                    prog = t / (epochs/10)
                    print("Progress: ", int(prog), "/ 10")
            # Reset to starting position
            wrld.setPosition(0,0,0)
            for _ in range(max_steps):
                    # Check whether state is a finish TODO: Finish this part
                    current_Y, current_X, current_V = wrld.current_pos
                    #print(current_Y, current_X)
                    current_node = wrld.node_map[current_Y][current_X][current_V]
                    if current_node.finish:
                        break
                    # See available actions
                    possible_actions = current_node.availablePaths()
                    # Pick the one with the best Q value (random if multiple the same)
                    if random.random() > random_threshold:
                            temp = min(possible_actions, key=possible_actions.get)
                            lowest_value = possible_actions[temp]
                            list_of_mins = []
                            for k in possible_actions.keys():
                                if possible_actions[k] == lowest_value:
                                    list_of_mins.append(k)
                            best_action = random.choice(list_of_mins)
                    else:
                            best_action = random.choice(list(possible_actions.keys()))
                    next_node = wrld.tryMove(best_action)
                    # Update it's value to the formula: Q(state, action) += alpha*(R(next_state) + gamma* max(Q(next_state, action)) - Q(state, action))
                    # Q(state, action)
                    current_Q = current_node.paths[best_action]
                    # R(next_state)
                    reward = next_node.reward
                    # Max(Q(next_sate, action))
                    next_actions = next_node.availablePaths()
                    next_action = min(next_actions, key=next_actions.get)
                    max_value = next_node.paths[next_action]
                    # Equation
                    new_value = current_Q + alpha * (reward + gamma * max_value - current_Q)
                    wrld.node_map[current_Y][current_X][current_V].setPath(best_action, new_value)
                    # Change to the next state
                    next_Y, next_X, next_V = next_node.id
                    wrld.setPosition(next_Y, next_X, next_V)

    # Check all the values
    # print("Q values: ")
    # for y in range(size_Y):
    #         for x in range(size_X):
    #             for v in range(2):
    #                 print("Y: ", y, " X: ", x, " V: ", v, wrld.node_map[y][x][v].availablePaths())

    # Check the chosen path
    # print("Chosen final path: ")
    print("Calculating the chosen path...")
    wrld.setPosition(0,0,0)
    total_cost = 0
    chosen_path = []
    for _ in range(10000):
            # Check if current node is the end
            current_Y, current_X, current_V = wrld.current_pos
            if (current_Y, current_X, current_V) in chosen_path:
                print("Didn't found an exit, in loop")
                break
            chosen_path += [(current_Y, current_X, current_V)]
            current_node = wrld.node_map[current_Y][current_X][current_V]
            if current_node.finish:
                break
            # See available actions
            possible_actions = current_node.availablePaths()
            # Pick the one with the best Q value (random if multiple the same)
            best_action = min(possible_actions, key=possible_actions.get)
            next_node = wrld.tryMove(best_action)
            # Change to the next state
            total_cost += next_node.reward
            next_Y, next_X, next_V = next_node.id
            wrld.setPosition(next_Y, next_X, next_V)
    elapsed_time = time.time() - start_time
    print("Parameters: ", par)
    print("Chosen path: ",chosen_path)
    print("Steps: ", len(chosen_path))
    print("Time estimate: ", len(chosen_path)*2.20, " | Total Reward: ", total_cost)
    print("Computation time: ", elapsed_time)
    print("-------\n")
