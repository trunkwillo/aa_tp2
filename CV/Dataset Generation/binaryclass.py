from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.layers import Dropout

classifier = Sequential()

classifier.add(Conv2D(32, (3, 3), input_shape = (64, 128, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))
classifier.add(Flatten())
classifier.add(Dense(units = 64, activation = 'relu'))
classifier.add(Dropout(0.5))
classifier.add(Dense(units = 1, activation = 'sigmoid'))
classifier.compile(optimizer = 'rmsprop', loss = 'binary_crossentropy', metrics = ['accuracy'])

from keras.preprocessing.image import ImageDataGenerator
train_datagen = ImageDataGenerator(rescale = 1./255, shear_range = 0.2, zoom_range = 0.2, horizontal_flip = True)
test_datagen = ImageDataGenerator(rescale = 1./255)
training_set = train_datagen.flow_from_directory('Train', target_size = (64, 128), batch_size = 32, class_mode = 'binary')
print(training_set.class_indices)
test_set = test_datagen.flow_from_directory('Test', target_size = (64, 128), batch_size = 32, class_mode = 'binary')
classifier.fit_generator(training_set, steps_per_epoch = 500, epochs = 20, validation_data = test_set, validation_steps = 500)
classifier.save('models/test5.h5')

import numpy as np
from keras.preprocessing import image
test_image = image.load_img('Test/pos/person_034.png', target_size = (64, 128))
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis = 0)
result = classifier.predict_classes(test_image)
print(result)