# import required packages
import sys
import os
import cv2
import numpy as np
from time import sleep
from json import dumps, loads
import base64
import numpy
import datetime

def crop_image(img, x, y, x_plus_w, y_plus_h):
    crop_img = img[y:y_plus_h, x:x_plus_w]
    current_date = datetime.datetime.now().strftime("%d-%m-%Y-%H:%M:%S:%f")
    cv2.imwrite('output/' + current_date + ".jpg",crop_img)

def get_output_layers(net):

    layer_names = net.getLayerNames()

    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    return output_layers

# function to draw bounding box on the detected object with class name
def draw_bounding_box(img, class_id, confidence, x, y, x_plus_w, y_plus_h):

    label = str(classes[class_id])

    color = COLORS[class_id]

    cv2.rectangle(img, (x,y), (x_plus_w,y_plus_h), color, 2)

    cv2.putText(img, label, (x-10,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
        print(base_path)
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

args_classes = resource_path("coco.names")
args_config = resource_path("yolov3.cfg")
args_weights = resource_path("yolov3.weights")

# read class names from text file
classes = None
with open(args_classes, 'r') as f:
    classes = [line.strip() for line in f.readlines()]

net = cv2.dnn.readNet(args_weights, args_config)


# TODO:
# - ler vídeo
# - processar frames de 1 em 1 segundo (for)
# - talvez seja preciso tirar draw_bounding_box

cap = cv2.VideoCapture('street.mp4')

count = 0
counter = 0
success = True

while(True):
    while success:
        success, img = cap.read()
        if count%30 == 0:
            success, img = cap.read()
            (Width, Height) = img.shape[:2]
            scale = 0.00392

            # generate different colors for different classes
            COLORS = np.random.uniform(0, 255, size=(len(classes), 3))

            # create input blob
            blob = cv2.dnn.blobFromImage(img, scale, (416,416), (0,0,0), True, crop=False)
            net.setInput(blob)

            # run inference through the network
            # and gather predictions from output layers
            outs = net.forward(get_output_layers(net))

            # initialization
            class_ids = []
            confidences = []
            boxes = []
            conf_threshold = 0.5
            nms_threshold = 0.4

            # for each detetion from each output layer
            # get the confidence, class id, bounding box params
            # and ignore weak detections (confidence < 0.5)
            for out in outs:
                for detection in out:
                    scores = detection[5:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]
                    if confidence > 0.5:
                        center_x = int(detection[0] * Width)
                        center_y = int(detection[1] * Height)
                        w = int(detection[2] * Width)
                        h = int(detection[3] * Height)
                        x = center_x - w / 2
                        y = center_y - h / 2
                        class_ids.append(class_id)
                        confidences.append(float(confidence))
                        boxes.append([x, y, w, h])

            # apply non-max suppression
            indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)

            # go through the detections remaining
            # after nms and draw bounding box

            objects = [0,0,0,0]

            for i in indices:
                i = i[0]
                box = boxes[i]
                x = box[0]
                y = box[1]
                w = box[2]
                h = box[3]

                #classes
                if (class_ids[i] == 0): # Pessoas
                    objects[3] += 1
                    crop_image(img, round(x), round(y), round(x+w), round(y+h))

                #draw_bounding_box(img, class_ids[i], confidences[i], round(x), round(y), round(x+w), round(y+h))

            print("Numero de Pessoas: " + str(objects[3]))        
        count+=1

    