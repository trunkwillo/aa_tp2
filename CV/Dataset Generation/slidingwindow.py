from helpers import pyramid
from helpers import sliding_window
import argparse
import time
import cv2


import numpy as np

from keras.models import load_model
model = load_model('models/test1.h5')
model.summary()

from keras.preprocessing import image
test_image = image.load_img('2.jpg', target_size = (64, 128))
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis = 0)
result = model.predict_classes(test_image)
print(result)

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

# load the image and define the window width and height
img = cv2.imread(args["image"])
img = cv2.line(img, (0,1000), (800,700), (147,96,44), 10)
img = cv2.line(img, (1800,1000), (1000,700), (147,96,44), 10)

(winW, winH) = (128, 256)

# loop over the image pyramid
for resized in pyramid(img, scale=1.5):
    # loop over the sliding window for each layer of the pyramid
    for (x, y, window) in sliding_window(resized, stepSize=128, windowSize=(winW, winH)):
        # if the window does not meet our desired window size, ignore it
        if window.shape[0] != winH or window.shape[1] != winW:
            continue

        window = np.resize(window, (64, 128, 3))
        test_image = image.img_to_array(window)
        test_image = np.expand_dims(test_image, axis=0)
        result = model.predict_classes(test_image)
        
        #if(result[0][0] == 1):
            #desenhar retangulo
            #cv2.rectangle(img, (x, y), (x + winW, y + winH), (0, 0, 255), 2)
            #print(result)

        #test_image = image.resize(64, 64)
        

        # since we do not have a classifier, we'll just draw the window
        clone = resized.copy()
        cv2.rectangle(clone, (x, y), (x + winW, y + winH), (0, 255, 0), 2)
        
        #clone = np.resize(clone, (64, 128, 3))
        #test_image = image.img_to_array(clone)
        #test_image = np.expand_dims(test_image, axis=0)
        #result = model.predict_classes(test_image)
        #print(result)

        cv2.imshow("Window", clone)
        cv2.waitKey(1)
        time.sleep(0.05)
    break

cv2.imshow('Window', img)
cv2.waitKey(0)